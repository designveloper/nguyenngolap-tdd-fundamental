# TDD - Test Driven Development
Intern: Nguyen Ngo Lap

## TDD in a nutshell
1. Write a test
2. Watch the test fail
3. Write application logic - as simple as possible
4. Pass the test
5. Refactor, removing duplication
6. Pass the test again

## 1. Introduction
Testing:

- Beta testing: Providing a finished or nearly finished app to a group of users and see what problems come up
- Performance testing: Using profiling tools to measure if we're getting acceptable response times
- Stress testing: Testing how well the code works under heavy load
- Integration testing: Testing an app integrating with external other systems
- Acceptance testing, regression testing, usability testing, etc.
- **Unit testing**: Testing the individual units of code. Test right from the start, test as we code

### What is TDD?

- Write the tests first before writing logic (Should write a failing test before writing new code :v)
- Focus on one test only - the exact one thing we should be doing at any one point

### Some common questions

**Does TDD work for everything?**

- No. E.g.: Multi-threading, security options, UI, some parts of game dev, etc.

**Am I supposed to write all the tests first?**

- No. We test as we code

**We have testers - do they write these tests?**

- No. They're for other kinds of test

## 2. Getting started
### Using unit testing frameworks

- Most of the testing frameworks are based on the same ideas - same common ancestor: SUnit (Kent Back) for SmallTalk
- SUnit for Smalltalk was converted into a version for Java - JUnit, the most popular unit testing framework
- After that: UNit (.NET), PyUnit (Python), CppUnit (C++), OCUnit(Objective-C), etc.
- The group of unit testing frameworks that came from the same idea is called *xUnit framework*
- Reference: https://en.wikipedia.org/wiki/List_of_unit_testing_frameworks

### Assertion

- State the fact - what you believe it should be at that point. If the assertion fails, the code is broken.

## 3. Working with tests
### The process of TDD
A quick summary:
- Red: Make a test that fails
- Green: Make that test pass
- Refactor: Make it right

**Notes:**

- Test methods should be considered independent of each other. Should be self-contained, isolated from others
- A good guideline is to create a seperate unit test, a separate class for each new class in the project
- **Naming test methods:** Use name of unit being tested, and add specifics. E.g.: 
    - `testWithdraw() {}`
    - `testWithdrawWithPenalty() {}`
    - `withdraw() {}` - Beware of different languages that need the `test` prefix
    - `withdraw_PenaltyAddedIfNegativeBalance() {}`
    - ...

## 4. Individual techniques
\- Each individual test can be summarized using 3 words - the 3 "a" rule:
    - Arrange: Set something up to manipulate
    - Act: Do something to it
    - Assert: Check the result

### Setup and Teardown
Avoiding duplication.

- Setup: What runs before each test
- Teardown: What runs after each test
- Setup Before Class: Run once before every test
- Teardown After Class: Run once after every test

\- The entire set of setUp, tearDown and test methods for a class is sometimes refered to as a **test fixture**

### Common questions

- No need to test methods like getters and setters. Only test the ones that can meaningfully break
- Generally can't test private methods
    - Some unit testing frameworks support it
    - Typically, just make a test for a public method that proves the private method works
- It's possible to bundle multiple test classes. It's usually referred to as a **test suite**
    - Most unit testing frameworks support this
- No need to (and shouldn't) control the order of tests
    - Controlling order suggests dependencies -> avoid
    - Unit tests are to test individual units of code in isolation, so the order is irrelevant

## 5. Additional topics
### Mock objects

- Assume that every other object (other classes, association, etc.) is perfect and reliable, so we can focus the test purely on the current unit -> can accurately identify the problem
=> Use mock object

- Replace other objects with fake (mock) implementations
- Typical reasons for Mock Objects:
    - Real object hasn't been written yet
    - What you're calling has a UI/needs human interaction
    - Slow or difficult to set up
    - External resource: file sys, database, network, printer

#### Fake objects vs Mock objects
**Fake objects:**

- The simplest type of mock object
- Match the original (or intended) method implementations
- Return pre-arranged results

**Mock objects:**

- When people distinguish between fake and mock, that means some code inside the mock object doesn't just return values, *but also verify interaction with the object it's mocking*
- Assert the expected values from unit under test (e.g.: verify the input into the mock object)

#### Mock object frameworks

- Provide structure for defining mock objects - somtimes removing the need to create a custom class
- Can aid in generating (or autogenerating) method stubs
- Often provide prearranged mock objects - for file stream, console, network, printer equivalents
- Reference:
    - http://www.jmock.org/
    - http://easymock.org/
    - http://site.mockito.org/
    - ...

### Code coverage

- A percentage measurement of exactly how much of the code is being successfully hit by the unit tests.
- Tools: EMMA, Clover, etc.

### TDD recommendtaions and next steps

**Numbers:**

- One test case/text fixture for each class
- 3-5 test methods for each class method
    - If it goes beyond or below, maybe reconsider the workload on that particular class

**What to test?**

- A test for every branch: if/else/and/or/case/for/while/polymorphism/...
- "Test until fear turns to boredom"
- Use code coverage tools

**What to avoid?**

- Interact with a database or file sys
- Require non-trivial network communication
- Require environment changes to run
- Call complex collaborator objects

=> Unit tests != other tests

**Adding tests to existing projects:**

- Approach #1: create a complete suite of unit tests (bad practice)
- Approach #2: create tests as needed
    - First, validate existing functionality
    - Then, Red/Green/Refactor for new functionality